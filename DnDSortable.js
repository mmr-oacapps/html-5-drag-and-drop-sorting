/**
 * @Author : MM Rahman
 * @Title : HTML 5 Drag and Drop Sorting
 * @Version : 1.0.1
 * @Description : HTML5 and native javascript plugin to implement drag and drop
 *              sorting
 * @BrowserSupport : Chrome 4.0 , IE 9.0, Firefox 3.5, Safari 6.0, Opera 12.0
 * @HowToUse : DnDsortable( document.getElementById('parentID'), function
 *           (item){ });
 */

function DnDSortable(parentElement, onUpdate) {

  var dragElement;

  [].slice.call(parentElement.children).forEach(function(itemEl) {
    itemEl.draggable = true;
  });

  /**
   * @Function _onDragOver
   * @params Event e
   * 
   */
  function _onDragOver(e) {

    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';

    var target = e.target;
    if (target && target !== dragElement && target.nodeName == 'DIV') {
      parentElement.insertBefore(dragElement, target.nextSibling || target);
    }
  };

  /**
   * @Function _onDragEnd
   * @params Event e
   * 
   */
  function _onDragEnd(e) {

    e.preventDefault();

    dragElement.classList.remove('dragOver');
    parentElement.removeEventListener('dragover', _onDragOver, false);
    parentElement.removeEventListener('dragend', _onDragEnd, false);

    onUpdate(dragElement);
  };

  parentElement.addEventListener('dragstart', function(e) {
    dragElement = e.target;

    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('Text', dragElement.textContent);

    parentElement.addEventListener('dragover', _onDragOver, false);
    parentElement.addEventListener('dragend', _onDragEnd, false);

    setTimeout(function() {
      dragElement.classList.add('dragOver');
    }, 0)
  }, false);
};